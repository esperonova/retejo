+++
linktitle = "Elŝutaĵoj"
title = "Elŝuti la mondmapon"
simbolo = "elŝuti"
[menu.cxefa]
    weight = 2
+++

# Elŝutaĵoj

## Kiel elŝuti kaj uzi la mondmapon

<div class="info">

### Antaŭ ol vi elŝutos la mondmapon

Antaŭ ol vi elŝutos la mondmapon, certiĝu ke via komputilo havas sufiĉan spacon por ĝi. La densigita dosiero havas proksimume 7.5GB, kiu fariĝas pli ol 12GB post maldensigo; tial, estas rekomendite ke via komputilo havu **almenaŭ 25GB** da libera spaco por konservi ĉiujn dosierojn.

Ankaŭ, pro ke la dosiero estas grandega, la elŝuto bezonos multe da tempo: ~20 minutojn per rapida konekto aŭ plurajn horojn per malrapida. Bonvolu havi paciencon.

</div>

1.  Elŝutu la mondmapon per la ligiloj sube.
2.  Movu la mondmapon en vian Minecraft-dosierujon, pli precize en la dosierujon “.minecraft/saves/”.
    (serĉu enrete “how to find minecraft folder” 🐊)
3.  Certiĝu ke vi uzas la ĝustan Minecraft version.
4.  Ekludu en unu-ludanta reĝimo. Se la mondo ĝuste montriĝas, la elŝuto estis sukcesa!

_Pli detalaj klarigoj aperos ĉi tie baldaŭ… kontrolu pli poste._

- EsperaMondo I  
  (pli malnova mondo, 1.12.2)
  [![EsperaMondo I](esperamondoI.png)](https://files.vpzom.click/EsperaMondoI.zip)

- EsperaMondo II  
  (malnova mondo, 1.15.2)
  [![EsperaMondo II](esperamondoII.png)](https://files.vpzom.click/EsperaMondoII.zip)

- EsperoNova
  (nuna mondo, 1.21.1)
  [![EsperoNova](esperonova.png)](https://io.esperonova.net/EsperoNova.zip)
