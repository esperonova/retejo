+++
linktitle = "Ekludi"
title = "Kiel ekludi en EsperoNova"
simbolo = "ekludi"
[menu.cxefa]
    weight = 5
+++

# Kiel ekludi en EsperoNova

<div class="grava-informo">

## 🔥 Grava informo − Legu antaŭ daŭrigi 🔥

Ne eblas ludi en EsperoNova se:

-   Vi ludas la poŝtelefonan version de Minecraft
-   Via komputilo aŭ retkonekto ne estas sufiĉe rapida

Ankaŭ, memoru ke **_EsperoNova estas komunumo por ESPERANTISTOJ_**.
Evidente vi estas esperantisto se vi povas legi ĉi tiun tekston sen tradukilo, tamen tio ankoraŭ direndas: **Ne invitu ne-Esperantistojn en la servilon; ludantoj kiuj ne esperantumas estos tuj forpelitaj**. Por pli da informo, bonvolu viziti la [servilan regularon](/regularo).

</div>
<div class="kontrollisto">

## 📝 Kontrollisto antaŭ ludado

Ludonte en EsperoNova, bonvolu kontroli la sekvajn aferojn:

-   [Legu la servilajn regulojn](/regularo). **Se vi ankoraŭ ne faris tion, faru nun**.
    La regularo estis tradukita en 6 lingvojn; ŝance vi trovos la regulojn en via denaska lingvo kaj en Esperanto.
-   Kontrolu ke vi uzas la ĝustan version de Minecraft (1.21.1).
-   Por pli Esperantuma sperto, estas rekomendate ŝanĝi vian ludlingvon al Esperanto dum vi ludas ĉe EsperoNova. Por ŝanĝi la ludlingvon, iru al <x-btn>Agordoj…</x-btn> → <x-btn>Lingvoj</x-btn> kaj elektu `Esperanto (Esperantujo)`.
-   Se vi havas malrapidan komputilon, estas rekomendate instali OptiFine antaŭ ludado. OptiFine estas modifaĵo, kiu ege plirapidigas onian Minecraft-klienton. 

</div>

---

## 🚀 Kiel ensaluti la unuan fojon

1.  Malfermu Minecraft, kaj alklaku <x-btn>Pluraj ludantoj</x-btn>.
    Se vi ankoraŭ ne havas Minecraft, vi povas aĉeti ĝin [ĉi tie](https://www.minecraft.net/en-us/get-minecraft).
    <span class="is-uppercase">Certiĝu ke vi uzas la ĝustan version de Minecraft (1.21.1)!</span>

    ![Malfermita Minecraft](malfermita-Minecraft.png)

2.  Alklaku <x-btn>Aldoni servilon</x-btn>.

    ![Aldoni servilon](aldoni-servilon.png)

3.  Tajpu la servilajn informojn en la tekstujoj, kaj aklaku <x-btn>Prete</x-btn>:  
    Servila nomo: `EsperoNova` (aŭ io ajn alia, laŭ via prefero)
    Servila adreso: `esperonova.net`

    ![Servilajn informojn](tajpi-adreson.png)

4.  Vi nun povas ekludi en EsperoNova!

    ![Preta](preta.png)

## 🏴‍☠️ Informo por piratoj

Uzantoj de neoficialaj kaj senpagaj klientoj povas eniri la servilon per la jena adreso:
`pirata.esperonova.net`

---

## ⌨️ Specialaj komandoj

EsperoNova havas multe da specialaj komandoj. Ĉi tiuj komandoj estas provizitaj per servila kromaĵo nomita _Movo_ & _Vortaro_, programita de la servilestroj mem. Jen kelkaj komandoj el tiu listo:

-   `/traduki <Esperanta vorto>` − Tradukas Esperantan vorton al onia vortara lingvo. Oni povas ŝanĝi onian vortaran lingvon per `/lingvo` (vidu sube). (_Ĉi tiu komando, kaj la komando sube, uzas la saman senkostan vortliston kiel [TujaVortaro.net](http://tujavortaro.net/)._)
-   `/lingvo <lingva kodo>` − Ŝanĝas la lingvon de onia vortaro. Subtenataj lingvoj estas samaj kiel tiuj de TujaVortaro.
-   `/aperejo` - Teleportas onin al la aperejo.
-   `/lito` - Teleportas onin al onia lito.
-   `/reen` − Teleportas onin al onia lasta teleporta loko. Se oni mortas enlude, ĉi tiu komando teleportas onin al la loko de la morto. Similas al la komando `/back` en aliaj serviloj.
-   `/aliri <ludanto>` − Sendas peton, demandante ĉu la sendinto rajtas teleporti al tiu ludanto.
-   `/venigi <ludanto>` − Simile al la supra komando, ĝi sendas venigan peton al tiu ludanto.
