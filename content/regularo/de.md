+++
title = "Deutsch"
lang = "de"
+++

# Server-Regeln

_EsperoNova hat sehr wenige Regeln: Wir vertrauen unseren Spielern, dass sie mit gesunden Menschenverstand und guten Urteilsvermögen handeln, damit der Server die friedliche kleine Ecke von Esperantujo bleibt, die es schon immer war.
Um diesen Frieden zu wahren, haben wir jedoch drei Grundregeln aufgestellt, die befolgt werden müssen:_

1.  **Verwende so viel wie möglich Esperanto.**
    Esperanto ist die einzige Sprache, die im öffentlichen Chat erlaubt ist.
    Wenn Du in anderen Sprachen kommunizieren möchtest, sende bitte private Nachrichten mit diesem Befehl: `/msg <Spieler> <Nachricht>`.
    Dies gilt auch für Anfänger; keine Ausreden!
2.  **Respektiere andere Spieler und ihre Builds.**
    Benutze gesunden Menschenverstand, genau wie auf jedem anderen Server. 
    Einige Spieler auf diesem Server haben buchstäblich _Hunderte von Stunden_ mit dem Bauen ihrer Builds verbracht. 
    Natürlich wäre jeder Spieler sauer, wenn er sich eines Tages einloggt und kaputte Builds oder geleerte Truhen vorfinden würde.
    Der Admin kann diesen Schaden ganz einfach reparieren; nichtsdestotrotz wäre es am besten, wenn diese Dinge gar nicht erst passieren würden.
    Griefing oder Stehlen kann zu einem Bann führen.
3.  **Lade nur andere Esperantisten auf dem Server ein.**
    Anfänger (_komencantoj_) sind auf dem Server immer willkommen; Tatsächlich ermutigen wir Anfänger aktiv, sich dem Server anzuschließen.
    Spieler, die kein Esperanto sprechen UND kein Interesse daran haben, es zu lernen, sind hier jedoch nicht willkommen; solche Spieler werden sofort gebannt.
    Esperanto-Sprecher dürfen keine Nicht-Esperantisten auf den Server einladen. Wenn Du dies versehentlich tust, erhälst Du eine Verwarnung, aber keine weitere Bestrafung.
    Wenn Du die Regel jedoch erneut brichst, führt dies zu einem dauerhaften Bann.
    (Bans können durch Kontaktaufnahme mit dem Administrator angefochten werden, siehe die Links unten.)

    <small>EsperoNova ist ein privat betriebener Server.  Der Administrator behält sich das Recht vor, jedem den Zugriff auf den Server ohne Vorankündigung zu verweigern.</small>	
