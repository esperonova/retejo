+++
title = "Esperanto"
lang = "eo"
+++

# Servilaj reguloj

_EsperoNova havas tre malmulte da reguloj: ni konfidas, ke la plejparto da ludantoj agos prudente, tiel la servilo restos la paca parto de Esperantujo, kiu ĝi nun estas.
Tamen, por konservi tiun pacon, ni ja havas kelkajn bazegajn regulojn por niaj ludantoj:_

1.  **Uzu Esperanton tiel multe kiel eble.**
    Nur Esperanto estas permesata en la publika babilejo.
    Se vi deziras uzi aliajn lingvojn, bonvolu fari tion en privataj mesaĝoj, per ĉi tiu komando: `/msg <ludanto> <mesaĝo>`.
2.  **Respektu aliajn ludantojn kaj iliajn konstruaĵojn.**
    Estas ludantoj ĉi tie, kiuj elspezis _centojn da horoj_ en siajn konstruaĵojn.
    Nature, ili koleriĝus se ili iutage ensalutus, kaj troviĝus rompitaj konstruaĵoj aŭ malplenigitaj kestoj.
    La servilestro povas facile ripari tian damaĝon; tamen, estus plej bone se tiuj malbonaĵoj neniam okazus.
    Estu prudenta!
    Se oni malrespektos la regulon, oni povos esti forbarita.
3.  **Nur invitu aliajn Esperantistojn en la servilon.**
    Komencantoj estas ĉiam bonvenaj en EsperoNova.
    Tamen, ludantoj kiuj nek parolas Esperanton, nek intencas lerni ĝin ne bonvenos, kaj estos tuj forpelitaj.
    _Bonvolu ne inviti tiajn ludantojn en la servilon._
    Se oni faros tion akcidente, oni estos nur avertita; se oni daŭre malrespektos la regulon, oni estos permanente forbarita.
    (Se oni estas forbarita, oni povas apelacii kontraŭ la forbaro kontaktinte la servilestron: vizitu la subajn ligilojn.)

<small>EsperoNova estas private operaciata servilo; ĉiu kaj ĉia forbaro el la servilo okazas sole laŭ la bontrovo de la servilestro.</small>
